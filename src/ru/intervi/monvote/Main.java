package ru.intervi.monvote;

import java.util.ArrayList;
import java.util.Random;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;

import ru.intervi.monvote.Users.UserData;

/**
 * Главный класс.
 */
public class Main extends JavaPlugin implements Listener {
	public Config config = new Config();
	public Core core = new Core(this);
	private HashMap<UUID, String[]> usermap = new HashMap<UUID, String[]>(); //выданные игроку ссылки
	
	@Override
	public void onEnable() {
		core.startTimer();
	}
	
	@Override
	public void onDisable() {
		core.stopTimer();
		clearAll();
	}
	
	private String[] getMons(boolean best) { //получить список мониторингов
		ArrayList<String> list = new ArrayList<String>((best) ? config.bestmons.keySet() : config.mons.keySet());
		if (config.random) {
			ArrayList<String> rlist = new ArrayList<String>();
			Random random = new Random();
			for (int i = 0; i < config.rlinks; i++) {
				int u = 0;
				while (true) { //отсечение одинаковых ссылок
					String url = list.get(random.nextInt(list.size()));
					if (!rlist.contains(url)) {
						rlist.add(url);
						break;
					}
					u++;
					if (u >= list.size() * 3) break; //страховка
				}
			}
			list = rlist;
		}
		return list.toArray(new String[list.size()]);
	}
	
	private void clearAll() { //очистить все хранилища
		core.parser.clear();
		usermap.clear();
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		usermap.remove(event.getPlayer().getUniqueId());
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (label.equalsIgnoreCase("vote")) {
			UUID uuid = null;
			boolean ingame = false;
			if (sender instanceof Player) {
				ingame = true;
				uuid = ((Player) sender).getUniqueId();
			}
			if (args == null || args.length == 0) {
				sender.sendMessage(config.help);
				return true;
			} else {
				switch(args[0].toLowerCase()) {
				case "get": //получить ссылки
					if (!ingame) {
						sender.sendMessage(config.onlygame);
						break;
					}
					if (!sender.hasPermission("monvote.get")) {
						sender.sendMessage(config.noperm);
						break;
					}
					UserData user = null;
					if (config.useuuid) user = Users.getUser(uuid);
					else user = Users.getUser(sender.getName());
					if (user != null && user.BAN) { //игрок забанен
						sender.sendMessage(config.banmess);
						break;
					}
					sender.sendMessage(config.text);
					String[] url = getMons(false);
					if (usermap.containsKey(uuid)) url = usermap.get(uuid);
					else usermap.put(uuid, url);
					sender.sendMessage(url);
					if (config.upmin <= 0) core.startParser((Player) sender, true, url);
					else core.parser.save(uuid, url);
					break;
				case "best": //получить ссылки на лучшие мониторинги
					if (!ingame) {
						sender.sendMessage(config.onlygame);
						break;
					}
					if (!sender.hasPermission("monvote.best")) {
						sender.sendMessage(config.noperm);
						break;
					}
					UserData buser = null;
					if (config.useuuid) buser = Users.getUser(uuid);
					else buser = Users.getUser(sender.getName());
					if (buser != null && buser.BAN) { //игрок забанен
						sender.sendMessage(config.banmess);
						break;
					}
					sender.sendMessage(config.besttext);
					String[] burl = getMons(true);
					if (usermap.containsKey(uuid)) burl = usermap.get(uuid);
					else usermap.put(uuid, burl);
					sender.sendMessage(burl);
					if (config.upmin <= 0) core.startParser(((Player) sender), true, burl);
					else core.parser.save(uuid, burl);
					break;
				case "done": //запуск проверки голосов
					if (!ingame) {
						sender.sendMessage(config.onlygame);
						break;
					}
					if (usermap.containsKey(uuid)) {
						sender.sendMessage(config.chdone);
						core.startParser(((Player) sender), false, usermap.get(uuid));
						usermap.remove(uuid);
					} else sender.sendMessage(config.nodone);
					break;
				case "help": //помощь
					sender.sendMessage(config.help);
					break;
				case "info": //вывод информации о игроке
					UUID iuuid = null;
					String iname = null;
					if (args.length < 2) { //о самом
						if (sender.hasPermission("monvote.info")) {
							iuuid = uuid;
							iname = sender.getName();
						} else {
							sender.sendMessage(config.noperm);
							break;
						}
					} else { //о другом
						if (sender.hasPermission("monvote.info-other")) {
							if (args[1].length() == 36) {
								try {
									iuuid = UUID.fromString(args[1]);
								} catch(Exception e) {}
							}
							if (iuuid == null) { //попытка нахождения по нику
								Player iplayer = Bukkit.getPlayer(args[1]);
								if (iplayer != null) iuuid = iplayer.getUniqueId();
							}
							iname = args[1];
						} else {
							sender.sendMessage(config.noperm);
							break;
						}
					}
					if ((config.useuuid && iuuid == null) || (!config.useuuid && iname == null)) { //нет ключей
						sender.sendMessage(config.nouser);
						break;
					}
					UserData iuser = null;
					if (config.useuuid) iuser = Users.getUser(iuuid); //через UUID
					else iuser = Users.getUser(iname); //через имя
					if (iuser == null) { //данные не получены
						sender.sendMessage(config.nouser);
						break;
					}
					sender.sendMessage(config.info[0].replaceAll("%name%", iname));
					sender.sendMessage(config.info[1].replaceAll("%votes%", String.valueOf(iuser.VOTES)));
					float time = (float) ((float) System.currentTimeMillis() / (short) 1000 / (short) 60 / (short) 60) -
							((float) iuser.STAMP / (short) 1000 / (short) 60 / (short) 60);
					sender.sendMessage(config.info[2].replaceAll("%hours%", String.valueOf(time)));
					sender.sendMessage(config.info[3].replaceAll("%ban%", String.valueOf(iuser.BAN)));
					break;
				case "reload": //перезагрузка конфига
					if (sender.hasPermission("monvote.reload")) {
						config.load();
						core.stopTimer();
						core.startTimer();
						sender.sendMessage(config.reload);
					} else sender.sendMessage(config.noperm);
					break;
				case "parse": //парсинг всех мониторингов
					if (sender.hasPermission("monvote.parse")) {
						sender.sendMessage(config.parsing);
						core.parser.parseAll();
					} else sender.sendMessage(config.noperm);
					break;
				case "clear": //очистка всех храналищ
					if (sender.hasPermission("monvote.clear")) {
						clearAll();
						sender.sendMessage(config.clear);
					} else sender.sendMessage(config.noperm);
					break;
				case "ban": //бан игрока
					if (sender.hasPermission("monvote.ban")) {
						if (args.length < 2) {
							sender.sendMessage(config.help);
							break;
						}
						if (config.useuuid) {
							UUID buuid = null;
							try {
								buuid = UUID.fromString(args[1]);
							} catch(Exception e) {}
							if (buuid == null) { //получение UUID, если игрок онлайн
								Player iplayer = Bukkit.getPlayer(args[1]);
								if (iplayer != null) buuid = iplayer.getUniqueId();
							}
							if (buuid == null) {
								sender.sendMessage(config.nouser);
								break;
							}
							Users.banUser(buuid);
							sender.sendMessage(config.ban.replaceAll("%name%", buuid.toString()));
						} else {
							Users.banUser(args[1]);
							sender.sendMessage(config.ban.replaceAll("%name%", args[1]));
						}
					} else sender.sendMessage(config.noperm);
					break;
				case "unban": //разбан игрока
					if (sender.hasPermission("monvote.unban")) {
						if (args.length < 2) {
							sender.sendMessage(config.help);
							break;
						}
						if (config.useuuid) {
							UUID buuid = null;
							try {
								buuid = UUID.fromString(args[1]);
							} catch(Exception e) {}
							if (buuid == null) { //получение UUID, если игрок онлайн
								Player iplayer = Bukkit.getPlayer(args[1]);
								if (iplayer != null) buuid = iplayer.getUniqueId();
							}
							if (buuid == null) {
								sender.sendMessage(config.nouser);
								break;
							}
							Users.unbanUser(buuid);
							sender.sendMessage(config.unban.replaceAll("%name%", buuid.toString()));
						} else {
							Users.unbanUser(args[1]);
							sender.sendMessage(config.unban.replaceAll("%name%", args[1]));
						}
					} else sender.sendMessage(config.noperm);
					break;
				default:
					sender.sendMessage(config.help);
				}
			}
		}
		return true;
	}
}
