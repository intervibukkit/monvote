package ru.intervi.monvote;

import java.io.File;
import java.util.HashMap;

import org.bukkit.ChatColor;

import ru.intervi.littleconfig.utils.Utils;
import ru.intervi.littleconfig.ConfigLoader;

public class Config {
	public Config() {
		load();
	}
	
	//ключ - ссылка, значение - класс элемента с цифрой голосов
	public HashMap<String, String> mons = new HashMap<String, String>();
	//ключ - число, значение - список команд (призы-ачивки)
	public HashMap<Integer, String[]> awards = new HashMap<>();
	//ключ - ссылка, значение - список команд
	public HashMap<String, String[]> bestmons = new HashMap<String, String[]>();
	public String[] award = null;
	public boolean random = false;
		public int rlinks = 2;
	public boolean flood = true;
		public String[] msg = null;
		public int interval = 20; //интервал в минутах
		public int votepause = 24; //пауза для флудера в часах
	public boolean useuuid = true;
	public boolean log = true;
	public boolean logger = true;
	public int upmin = 5; //чтобы отключить обновлялку - 0
	public int attempts = 3; //кол-во попыток парсинга
	public String useragent = "Mozilla/5.0 (Windows; U; WindowsNT 5.1; ru-RU; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6";
	public String referrer = "http://www.google.com";
	
	public String bvote = "%name% получил приз за голосование";
	public String onlygame = "эту команду можно выполнять только в игре";
	public String chdone = "проверка...";
	public String nodone = "сначала нужно написать /vote get";
	public String noperm = "нет прав";
	public String nouser = "игрок не найден";
	public String parsing = "парсинг...";
	public String clear = "очищено";
	public String ban = "%name% забанен";
	public String unban = "%name% разбанен";
	public String banmess = "ты забанен";
	public String reload = "конфиг перезагружен";
	public String novote = "ты не проголосовал";
	public String[] help = null;
	public String[] text = null;
	public String[] besttext = null;
	public String[] info = null;
	
	private String color(String str) {
		return ChatColor.translateAlternateColorCodes('&', str);
	}
	
	private String[] color(String[] str) {
		String[] result = new String[str.length];
		for (int i = 0; i < str.length; i++) result[i] = color(str[i]);
		return result;
	}
	
	public void load() {
		//создание конфига
		String sep = File.separator;
		String path = Utils.getFolderPath(this.getClass()) + sep + "MonVote";
		File dir = new File(path); //проверка папки
		if (!dir.isDirectory()) dir.mkdirs();
		path += sep + "config.yml";
		File file = new File(path);
		if (!file.isFile()) Utils.saveFile(this.getClass().getResourceAsStream("/config.yml"), file);
		//загрузка
		ConfigLoader loader = null;
		try {
			loader = new ConfigLoader(file, false);
		} catch(Exception e) {e.printStackTrace();}
		if (loader == null) return;
		//заполнение
		random = loader.getBoolean("random");
		flood = loader.getBoolean("flood");
		useuuid = loader.getBoolean("useuuid");
		log = loader.getBoolean("log");
		logger = loader.getBoolean("logger");
		rlinks = loader.getInt("rlinks");
		interval = loader.getInt("interval");
		votepause = loader.getInt("votepause");
		upmin = loader.getInt("upmin");
		attempts = loader.getInt("attempts");
		mons.clear();
		awards.clear();
		bestmons.clear();
		for (String key : loader.getSectionVars("mons")) {
			mons.put(key, loader.getStringInSection("mons", key));
		}
		for (String key : loader.getSectionVars("awards")) {
			awards.put(Integer.parseInt(key), loader.getStringArrayInSection("awards", key));
		}
		for (String key : loader.getSectionVars("bestmons")) {
			bestmons.put(key, loader.getStringArrayInSection("bestmons", key));
		}
		useragent = loader.getString("useragent");
		referrer = loader.getString("referrer");
		bvote = color(loader.getString("bvote"));
		onlygame = color(loader.getString("onlygame"));
		chdone = color(loader.getString("chdone"));
		nodone = color(loader.getString("nodone"));
		noperm = color(loader.getString("noperm"));
		nouser = color(loader.getString("nouser"));
		parsing = color(loader.getString("parsing"));
		clear = color(loader.getString("clear"));
		ban = color(loader.getString("ban"));
		unban = color(loader.getString("unban"));
		banmess = color(loader.getString("banmess"));
		reload = color(loader.getString("reload"));
		novote = color(loader.getString("novote"));
		award = loader.getStringArray("award");
		msg = color(loader.getStringArray("msg"));
		help = color(loader.getStringArray("help"));
		text = color(loader.getStringArray("text"));
		besttext = color(loader.getStringArray("besttext"));
		info = color(loader.getStringArray("info"));
	}
}
