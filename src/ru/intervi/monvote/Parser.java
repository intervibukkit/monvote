package ru.intervi.monvote;

import java.util.concurrent.ConcurrentHashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Element;

/**
 * Парсер монитрингов.
 */
public class Parser {
	public Parser(Main main) {
		this.main = main;
	}
	
	private Main main;
	private volatile ConcurrentHashMap<String, Integer> map = new ConcurrentHashMap<String, Integer>(); //голоса
	//старые голоса, при выдачи ссылок игроку
	private volatile ConcurrentHashMap<UUID, HashMap<String, Integer>> votes = new ConcurrentHashMap<UUID, HashMap<String, Integer>>();
	private Timer timer = null;
	private Updater task = null;
	
	private class Updater extends TimerTask {
		@Override
		public void run() {
			parseAll();
		}
	}
	
	/**
	 * сохранить голоса из мониторинга
	 * @param url ссылка
	 */
	public void parse(String url) {
		if (!main.config.mons.containsKey(url)) return;
		Document doc = null;
		for (int i = 0; i < main.config.attempts; i++) {
			try {
				Connection con = Jsoup.connect(url);
				if (main.config.useragent != null && main.config.useragent.length() > 1)
					con.userAgent(main.config.useragent);
				if (main.config.referrer != null && main.config.referrer.length() > 1)
					con.referrer(main.config.referrer);
				doc = con.get();
				break;
			} catch(Exception e) {e.printStackTrace();}
		}
		if (doc != null) {
			String id = main.config.mons.get(url);
			int pos = -1; //чтение заданного по счету элемента
			if (id.indexOf(':') != -1) { //в конфиге прописывается через двоеточие
				String[] aid = id.split(":");
				id = aid[0].trim();
				try {
					pos = Integer.parseInt(aid[1].trim());
				} catch(Exception e) {e.printStackTrace();}
			}
			Elements els = doc.getElementsByClass(id);
			if (els.size() == 0) els = new Elements(doc.getElementById(id));
			for (Element e : els) { //получение элементов по классу
				if (pos != -1) e = els.get(pos); //получение заданного элемента
				if (!e.hasText() && pos == -1) continue;
				if (!e.hasText() && pos != -1) break;
				Pattern pattern = Pattern.compile("\\d+");
				Matcher matcher = pattern.matcher(e.text().trim());
				int votes;
				if (matcher.find()) votes = Integer.parseInt(matcher.group());
				else continue;
				if (map.containsKey(url)) map.replace(url, votes);
				else map.put(url, votes);
				if (pos != -1) break;
			}
		}
	}
	
	/**
	 * сохранить голоса из всех мониторингов
	 */
	public void parseAll() {
		for (String url : main.config.mons.keySet()) parse(url);
	}
	
	/**
	 * сохранить старые голоса под голосующего
	 * @param uuid
	 * @param url ссылки
	 */
	public void save(UUID uuid, String[] url) {
		HashMap<String, Integer> hmap = new HashMap<String, Integer>();
		for (String u : url) {
			if (map.containsKey(u)) hmap.put(u, map.get(u));
		}
		votes.put(uuid, hmap);
	}
	
	/**
	 * сохранить все старые голоса под голосующего
	 * @param uuid
	 */
	public void saveAll(UUID uuid) {
		save(uuid, map.keySet().toArray(new String[map.size()]));
	}
	
	/**
	 * проверить голоса в мониторинге
	 * @param uuid
	 * @param url ссылка на страницу
	 * @return true если голосов стало больше
	 */
	public boolean check(UUID uuid, String url) {
		if (!map.containsKey(url) || (!votes.containsKey(uuid) || !votes.get(uuid).containsKey(url))) return false;
		int old = votes.get(uuid).get(url).intValue(); //старое кол-во голосов
		parse(url);
		int nw = map.get(url).intValue();
		if (nw > old) return true;
		else return false;
	}
	
	/**
	 * проверить голоса во всех мониторингах
	 * @param uuid
	 * @return в скольких мониторингах голосов стало больше
	 */
	public int checkAll(UUID uuid) {
		Map<String, Integer> old = votes.get(uuid);
		parseAll();
		int result = 0;
		for (String url : map.keySet()) {
			if (!old.containsKey(url)) continue; //на случай ошибок
			if (map.get(url) > old.get(url)) result++;
		}
		return result;
	}
	
	/**
	 * очистка данных
	 */
	public void clear() {
		map.clear();
		votes.clear();
	}
	
	/**
	 * удалить старые голоса под игрока
	 * @param uuid
	 */
	public void remove(UUID uuid) {
		votes.remove(uuid);
	}
	
	/**
	 * запуск таймера
	 */
	public void startTimer() {
		if (main.config.upmin <= 0) return;
		timer = new Timer();
		task = new Updater();
		long interval = main.config.upmin * 60 * 1000;
		timer.schedule(task, 0, interval);
	}
	
	/**
	 * остановка таймера
	 */
	public void stopTimer() {
		if (timer != null && task != null) {
			timer.cancel();
			timer = null;
			task = null;
		}
	}
}
