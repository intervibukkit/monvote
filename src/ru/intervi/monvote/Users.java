package ru.intervi.monvote;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import ru.intervi.littleconfig.utils.Utils;
import ru.intervi.littleconfig.ConfigLoader;
import ru.intervi.littleconfig.ConfigWriter;

/**
 * Чтение и запись данных игрока.
 */
public class Users {
	private final static File FOLDER = new File(Utils.getFolderPath(Config.class) + File.separator + "MonVote" + File.separator + "userdata");
	
	/**
	 * Контейнер с данными игрока.
	 */
	public static class UserData {
		public UserData(int votes, long stamp, boolean ban) {
			VOTES = votes;
			STAMP = stamp;
			BAN = ban;
		}
		
		public final int VOTES;
		public final long STAMP;
		public final boolean BAN;
	}
	
	private static UserData getUserData(String key) {
		if (!FOLDER.isDirectory()) return null;
		String path = FOLDER.getAbsolutePath() + File.separator + key + ".yml";
		File file = new File(path);
		if (!file.isFile()) return null;
		ConfigLoader loader = new ConfigLoader();
		try {
			loader.load(file, false);
		} catch(IOException e) {e.printStackTrace();}
		return new UserData(loader.getInt("votes"), loader.getLong("stamp"), loader.getBoolean("ban"));
	}
	
	public static UserData getUser(String name) {
		return getUserData(name.toLowerCase());
	}
	
	public static UserData getUser(UUID uuid) {
		return getUserData(uuid.toString());
	}
	
	private static void saveUserData(String key, UserData user) {
		if (!FOLDER.isDirectory()) FOLDER.mkdir();
		String path = FOLDER.getAbsolutePath();
		File data = new File(path);
		if (!data.isDirectory()) data.mkdir();
		String pathfile = path + File.separator + key + ".yml";
		ConfigWriter writer = new ConfigWriter();
		try {
			writer.setConfig(pathfile, false);
		} catch(Exception e) {e.printStackTrace();}
		writer.setOption("votes", String.valueOf(user.VOTES));
		writer.setOption("stamp", String.valueOf(user.STAMP));
		writer.setOption("ban", String.valueOf(user.BAN));
		try {
			writer.writeFile();
		} catch(Exception e) {e.printStackTrace();}
	}
	
	public static void saveUser(String name, UserData user) {
		saveUserData(name.toLowerCase(), user);
	}
	
	public static void saveUser(UUID uuid, UserData user) {
		saveUserData(uuid.toString(), user);
	}
	
	private static void updateUserVotes(String key, int votes) {
		UserData user = getUserData(key);
		if (user == null) saveUserData(key, new UserData(votes, System.currentTimeMillis(), false));
		else saveUserData(key, new UserData(user.VOTES+votes, System.currentTimeMillis(), user.BAN));
	}
	
	public static void updateVotes(String name, int votes) {
		updateUserVotes(name.toLowerCase(), votes);
	}
	
	public static void updateVotes(UUID uuid, int votes) {
		updateUserVotes(uuid.toString(), votes);
	}
	
	public static void banUser(String name) {
		UserData user = getUserData(name);
		if (user == null) saveUserData(name, new UserData(0, System.currentTimeMillis(), true));
		else saveUserData(name, new UserData(user.VOTES, user.STAMP, true));
	}
	
	public static void banUser(UUID uuid) {
		banUser(uuid.toString());
	}
	
	public static void unbanUser(String name) {
		UserData user = getUserData(name);
		if (user == null) saveUserData(name, new UserData(0, System.currentTimeMillis(), false));
		else saveUserData(name, new UserData(user.VOTES, user.STAMP, false));
	}
	
	public static void unbanUser(UUID uuid) {
		banUser(uuid.toString());
	}
}
