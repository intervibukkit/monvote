package ru.intervi.monvote;

import java.util.UUID;
import java.util.Timer;
import java.util.TimerTask;
import java.io.File;

import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

import ru.intervi.monvote.Users.UserData;

import ru.intervi.littleconfig.utils.Utils;
import ru.intervi.littleconfig.utils.EasyLogger;

/**
 * Основной класс, через который осуществляется работа с мониторингами.
 */
public class Core implements Listener {
	public Core(Main main) {
		this.main = main;
		this.parser = new Parser(main);
		logger.offLog();
	}
	
	private Main main;
	public Parser parser;
	private Timer timer = null;
	private Flooder task = null;
	private EasyLogger logger = new EasyLogger("[MonVote]", new File(Utils.getFolderPath(this.getClass()) + File.separator + "MonVote" + File.separator + "vote.log"));
	
	/**
	 * Парсер, выполняющий все необходимые действия после проверки голосов.
	 */
	public class Parsing extends Thread {
		/**
		 * 
		 * @param player игрок
		 * @param parse true если нужно только обновить данные
		 * @param url адреса мониторингов
		 */
		public Parsing(Player player, boolean parse, String[] url) {
			this.player = player;
			this.parse = parse;
			this.url = url;
		}
		
		private final boolean parse;
		private final String[] url;
		private Player player;
		
		@Override
		public void run() {
			if (parse) { //обновление данных
				for (String u : url) {
					for (int i = 0; i < main.config.attempts; i++) {
						try {
							parser.parse(u);
							break;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				parser.save(player.getUniqueId(), url);
			} else { //проверка голосов
				String name = player.getName();
				UUID uuid = player.getUniqueId();
				boolean result = false; //прибавились ли голоса
				int mons = 0; //кол-во голосов (1 на каждый мониторинг)
				int bmons = 0; //кол-во голосов в лучших мониторингах
				for (String u : url) {
					if (parser.check(uuid, u)) { //если голосов прибавилось
						if (main.config.bestmons.containsKey(u)) {
							if (player == null || !player.isOnline()) return;
							for (String com : main.config.bestmons.get(u)) { //выдача супер-призов за лучшие мониторинги
								Bukkit.dispatchCommand(Bukkit.getConsoleSender(), com.replaceAll("%name%", name));
							}
							mons--; //чтобы не выдавался простой приз
							bmons++;
						}
						mons++;
						result = true;
					}
				}
				int allmons = mons + bmons;
				parser.remove(uuid);
				if (player == null || !player.isOnline()) return;
				if (result) {
					int votes = 0; //сколько всего голосов
					//обновление данных о голосах игрока
					if (main.config.useuuid) {
						Users.updateVotes(uuid, allmons);
						votes = Users.getUser(uuid).VOTES;
					} else {
						Users.updateVotes(name, allmons);
						votes = Users.getUser(name).VOTES;
					}

					for (int i = 0; i < mons; i++) { //выдача призов согласно кол-ву мониторингов
						for (String com : main.config.award)
							Bukkit.dispatchCommand(Bukkit.getConsoleSender(), com.replaceAll("%name%", name));
					}

					/* FOR FUTURE
					String[] award = null;
					int max = -1;
					for (Map.Entry<Integer, String[]> entry : main.config.awards.entrySet()) {
						if (votes < entry.getKey() || entry.getKey() < max) continue;
						max = entry.getKey();
						award = entry.getValue();
					}
					 */
					String[] award = main.config.awards.get(votes);
					if (award != null) { //выдача достижений
						for (String com : award)
							Bukkit.dispatchCommand(Bukkit.getConsoleSender(), com.replaceAll("%name%", name));
						String mess = name + " полчил достижение (" + String.valueOf(votes) + " голосов)";
						if (main.config.log) Bukkit.getLogger().info(mess);
						if (main.config.logger) logger.error(mess);
					}

					String mess = name + " успешно проголосовал (в " + String.valueOf(allmons) + " мониторингах | " + String.valueOf(votes) + " всего)";
					if (main.config.log) Bukkit.getLogger().info(mess);
					if (main.config.logger) logger.info(mess);
					if (main.config.bvote != null && main.config.bvote.length() > 1) //оповещение игрокам
						Bukkit.broadcast(main.config.bvote.replaceAll("%name%", name), "monvote.see");
				} else {
					player.sendMessage(main.config.novote);
					if (main.config.log) Bukkit.getLogger().info(name + " не проголосовал");
					if (main.config.logger) logger.error(name + " не проголосовал");
				}
			}
		}
	}
	
	/**
	 * Рассылка рекламных оповещений.
	 */
	private class Flooder extends TimerTask {
		@Override
		public void run() {
			long stamp = System.currentTimeMillis();
			long mins = stamp / 1000 / 60;
			long hours = mins / 60;
			for (Player player : Bukkit.getOnlinePlayers()) {
				if (player.hasPermission("monvote.flood")) {
					UserData user = null;
					UUID uuid = player.getUniqueId();
					String name = player.getName();
					if (main.config.useuuid) user = Users.getUser(uuid);
					else user = Users.getUser(name.toLowerCase());
					if (user != null) {
						if (user.BAN) continue; //проверка на бан
						long oldhours = user.STAMP / 1000 / 60 / 60;
						if (hours - oldhours < main.config.votepause) continue; //проверка на паузу
					}
					for (String line : main.config.msg) //рассылка
						player.sendMessage(line.replaceAll("%name%", name));
				}
			}
		}
	}
	
	public void startTimer() {
		timer = new Timer();
		task = new Flooder();
		long interval = main.config.interval * 60 * 1000;
		timer.schedule(task, 0, interval);
		parser.startTimer();
	}
	
	public void stopTimer() {
		if (timer != null && task != null) {
			timer.cancel();
			timer = null;
			task = null;
		}
		parser.stopTimer();
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		parser.remove(event.getPlayer().getUniqueId());
	}
	
	public void startParser(Player player, boolean parse, String[] url) {
		Parsing parsing = new Parsing(player, parse, url);
		parsing.start();
	}
}
